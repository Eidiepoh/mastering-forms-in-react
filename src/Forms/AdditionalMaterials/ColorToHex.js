const colors = ["White", "Red", "Lime", "Blue", "Yellow", "Cyan", "Magenta", "Silver", "Gray", "Maroon", "Olive", "Green", "Purple", "Teal", "Navy"]

const colorsToHex = {
    White: '#FFFFFF',
    Red: '#FF0000',
    Lime: '#00FF00',
    Blue: '#0000FF',
    Yellow: '#FFFF00',
    Cyan: '#00FFFF',
    Magenta: '#FF00FF',
    Silver: '#C0C0C0',
    Gray: '#808080',
    Maroon: '#800000',
    Olive: '#808000',
    Green: '#008000',
    Purple: '#800080',
    Teal: '#008080',
    Navy: '#000080'
}

const ConvertColor = (input) => {
  return  colorsToHex[input]
}

export   { colors, ConvertColor}
