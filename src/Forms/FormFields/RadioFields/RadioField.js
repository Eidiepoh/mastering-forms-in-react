import { FormControlLabel, Radio, InputLabel } from '@mui/material';
import RadioGroup from '@mui/material/RadioGroup';
import { useField, Field } from 'formik';

const  RadioField = ({label, inputs, ...props})  => {
    const [field] = useField(props);
    const inputsObj = inputs.map((item, index) => ({id: index, item: item}));

    return (
      <div className="fields radio">
        <InputLabel className="radio label">{label}</InputLabel>        
          <div>
            <RadioGroup className="radios">  
            {inputsObj.map((item) => 
                <Field as={FormControlLabel}  control={<Radio/>}   
                {...field}
                key={item.id}
                label={item.item}
                value={item.item}/>
            )}  
          </RadioGroup>  
          </div>
      </div>
    )
  }

export default RadioField
