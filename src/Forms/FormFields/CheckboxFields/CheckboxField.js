import { Checkbox, InputLabel, FormControlLabel } from '@mui/material';
import { Field, useField, ErrorMessage } from 'formik';

const CheckboxField = ({label, inputs=[''], ...props}) => {
    const [field] = useField(props);
    const inputsObj = inputs.map((item, index) => ({id: index, item: item}));

    return (
        <div className="fields checkbox" >
            <InputLabel className="checkbox label">{label}</InputLabel>
                <div className="checkboxes">
                    <ErrorMessage name={field.name}/>
                        {inputsObj.map((item) =>
                            <Field 
                            {...field}
                            as={FormControlLabel}  
                            key={item.id + '' + item.item}
                            value={item.item}
                            label={item.item} 
                            control = {
                                <Field as={Checkbox} id={String('checkbox ' + item.item)} type='checkbox'/>
                            }/>
                        )}
            </div>
        </div>    
    )
}

export default  CheckboxField;
