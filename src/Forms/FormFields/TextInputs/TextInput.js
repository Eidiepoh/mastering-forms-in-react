import React, {useState} from 'react';
import {  InputLabel, TextField } from '@mui/material';
import { Field, useField, ErrorMessage } from 'formik';
import '../Fields.css'

const TextInput = ({label, ...props}) => {
    const [inputValue, setInputValue] = useState(0)
    
    const handleChange = (e) => {
        setInputValue(e.target.textLength)
    }
    const [field] = useField(props);
    
    return (
        <div className="fields text" >
            <InputLabel className="text label">{label}</InputLabel>
            <div>
                <p className="character-counter">{`${inputValue}/100`}</p>
                <Field className="texts"
                    inputProps={{onChange:handleChange}}
                    as={TextField} {...field} label={label}
                    multiline
                    size="small"/>
                <ErrorMessage component="div" name={field.name} className="error"/>
            </div>
        </div>
    )
}

export default  TextInput;
