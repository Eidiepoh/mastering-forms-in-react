import React from 'react';
import { TextField, InputLabel } from '@mui/material';
import { Field, useField, ErrorMessage } from 'formik';
import '../Fields.css';
import '../error.css';

const InputField = ({label, ...props}) => {
    const [field, meta] = useField(props);
    
    return (
        <div className="fields input" >
            <InputLabel className="input label">{label}</InputLabel>
            <div>              
                <Field error={meta.touched && meta.error && true} 
                    color={`warning`}
                    className="inputs" 
                    as={TextField} 
                    {...field}  
                    label={label}  
                    size="small"/>
                <ErrorMessage component="div" name={field.name} className="error"/>
            </div>      
        </div>
    )
}

export default  InputField;
