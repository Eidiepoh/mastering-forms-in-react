import Button from '@mui/material/Button';

const ButtonComp = ({variant, type, disable, ...props}) => {
    return (
        <Button variant={variant} disabled={disable} type={type} >{props.children}</Button>
    )
}

export default  ButtonComp;
