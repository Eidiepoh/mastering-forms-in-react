import { InputLabel, MenuItem, Select } from '@mui/material';
import { useField, Field, ErrorMessage } from 'formik';

const SelectField  = ({label, inputs, ...props}) => {
    const [field, meta] = useField(props);
    const inputsObj = inputs.map((item, index) => ({id: index, item: item}));

    return (
        <div className="fields select">
            <InputLabel className="select label">{label}</InputLabel>
            <div>
                <Field className="selects" size="small"
                    error={meta.touched && meta.error && true}
                    label={`${meta.touched && meta.error && 'Outlined secondary'}`}
                    color={`warning`}                
                    as={Select}
                    name={field.name} >
                
                    {inputsObj.map(item => 
                    <Field as={MenuItem} 
                    {...field}
                    id={item.id}
                    key={item.id}
                    value={item.item}>{item.item}</Field>)}
                </Field>   
            <ErrorMessage component="div" name={field.name} className="error"/>
            </div> 
        </div>
    )
}

export default SelectField;
