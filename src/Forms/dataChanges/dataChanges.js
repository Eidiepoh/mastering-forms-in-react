import { ConvertColor } from '../AdditionalMaterials/ColorToHex';

const Datachanges = ({data}) => {
    const filteredData = {};
    /* constructing dynamic JSON object, 
    representing current form data status */
    for(let [index, item] of Object.entries(data)) {
        if(index === 'employed') filteredData[index] = 'false';
        if(item != false) {
            filteredData[index] = item;
        }
    }
    // converting color name into hex representation
   filteredData.favoriteColor = ConvertColor(filteredData.favoriteColor);
    return (
        <pre className={`data-presentation`}>{JSON.stringify(filteredData, null, 2)}</pre>
    )
}

export default Datachanges;