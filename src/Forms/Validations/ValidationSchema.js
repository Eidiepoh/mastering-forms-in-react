import * as yup from 'yup';

const validationSchema = yup.object({
    firstName: yup
    .string()
    .required('Please fill out this field!')
    .matches(/^[A-Za-z]+$/,'Invalid characters, please use letters only'),
    lastName: yup
    .string()
    .required('Please fill out this field')
    .matches(/^[A-Za-z]+$/,'Invalid characters, please use letters only') ,
    age: yup
    .number()
    .required('Please fill out this field')
    .typeError('Please enter only numbers')
    .positive('Must be greater than zero')
    .integer('Please enter valid number')
    .max(150, 'You entered too high number'),
    favoriteColor: yup
    .string().required('Please choose a color'),
    notes: yup.string().max(100, 'Maximum character size is 100') 
})

export default validationSchema;
