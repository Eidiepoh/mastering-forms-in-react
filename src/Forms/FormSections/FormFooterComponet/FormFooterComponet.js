import Button from '../../FormFields/Buttons/Button';
import Datachanges from '../../dataChanges/dataChanges';
import './FormFooterComponet.css'

const FormFooterComponet = ({data, disableSubmit, disableReset}) => {

    return (
        <div className="bottom-section">
        
            <div className="buttons-duo">
                <Button  variant="contained" type="submit" disable={disableSubmit} >Submit</Button>
                <Button  variant="outlined" type="reset" disable={disableReset} >Reset</Button>
            </div>
            <Datachanges data={data}/>
        </div>

    )
}

export default FormFooterComponet;