import RadioField from '../../FormFields/RadioFields/RadioField';
import CheckboxField from '../../FormFields/CheckboxFields/CheckboxField';
import TextInput from '../../FormFields/TextInputs/TextInput';

const FormMainComponent = () => {
    return (
        <>
        <CheckboxField name="sauces" label="Sauces"   inputs={['Ketchup', 'Mustard', 'Mayonnaise', 'Guacamole']}/>
        <RadioField name="stooge" label="Best Stooge"  inputs={['Larry', 'Moe', 'Curly']}/>
        <TextInput name="notes" label="Notes"/>
    </>
    )
}

export default FormMainComponent;
