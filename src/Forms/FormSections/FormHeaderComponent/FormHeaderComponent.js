import InputField from '../../FormFields/InputFields/InputField';
import SelectField from '../../FormFields/SelectFields/SelectField';
import CheckboxField from '../../FormFields/CheckboxFields/CheckboxField';
import { colors } from '../../AdditionalMaterials/ColorToHex';


const FormHeaderComponent = () => {
    return (
        <>
        <InputField name="firstName" label="First Name" type="text" />
        <InputField name="lastName" label="Last Name" type="text" />
        <InputField name="age" label="Age" type="number" />
        <CheckboxField name="employed" label="Employed"  type='checkbox' />
        <SelectField name="favoriteColor" label="Favorite Color"  inputs={colors}/>
        </>
    )
}

export default FormHeaderComponent;
