import * as React from 'react';
import './Forms.css';
import { Formik, Form } from 'formik';
import validationSchema from './Validations/ValidationSchema'
import FormHeaderComponent from './FormSections/FormHeaderComponent/FormHeaderComponent';
import FormMainComponent from './FormSections/FormMainComponent/FormMainComponent';
import FormFooterComponet from './FormSections/FormFooterComponet/FormFooterComponet';
import { ConvertColor } from './AdditionalMaterials/ColorToHex';

const Forms = () => {
    // controlling submit button status
    let submitButtonStatus = true;
    // controlling reset button status
    let resetButtonStatus = false;
    
    return(
        <div className="Form">
        
            <Formik validateOnChange={false} validateOnBlur={false} initialValues={{
                firstName: '',
                lastName: '',
                age: '',
                employed: '',
                favoriteColor: '',
                sauces: [],
                stooge: '',
                notes: ''
            }}
            // applying validation rules, constructed using yup
            validationSchema = {validationSchema}

            onReset={() => {
                resetButtonStatus = true;
            }}
            
            onSubmit={  (data, { setSubmitting, resetForm }) => {
                setSubmitting(true);
                if(data.employed === '') {
                    data.employed = 'false'
                }
                data.favoriteColor = ConvertColor(data.favoriteColor);
                setTimeout(() => {
                    alert(JSON.stringify(data, null, 2));
                    setSubmitting(false);
                  }, 400);
                submitButtonStatus = true;
                resetForm();
            }}>

            {({ values, isSubmitting, handleSubmit, handleReset }) => (

                <Form onReset={handleReset}  onSubmit={handleSubmit} onChange={() => {
                    submitButtonStatus = false;
                    resetButtonStatus = false;
                }}>        
                    <FormHeaderComponent/>
                    <FormMainComponent/>
                    <FormFooterComponet
                    data={values}  
                    disableSubmit={  isSubmitting || submitButtonStatus }
                    disableReset={ resetButtonStatus }
                    />
                </Form>
            )}

            </Formik>
        </div>
    )
}

export default  Forms;
